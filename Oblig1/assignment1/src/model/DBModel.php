<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
            // Create PDO connection
            try
            {
            $this->db = new PDO('mysql:host=localhost;dbname=test1;charset=utf8', 
                                'admin', 'admin',  
                                 array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
            }
            catch (PDOException $e)
            {
                echo 'PDOException: ' . $e->getMessage();
            }
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
		$booklist = array();
        try 
        {
            $stmt = $this->db->query('SELECT * FROM booklist');

            while($row = $stmt->fetch(PDO::FETCH_ASSOC))
            {
                $booklist[] = new Book($row['title'], 
                                       $row['author'], 
                                       $row['description'], 
                                       $row['ID']);
            }
            
        } 
        catch (PDOException $e) 
        {
            echo 'PDOException: ' . $e->getMessage();
        }
        return $booklist;
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
        if (!is_numeric($id))
        {
            $id = -1;
        }
		$book = null;
        $stmt = $this->db->prepare('SELECT * FROM booklist WHERE ID = :id');
        $stmt->bindValue(':id', $id);

        try 
        {

            $stmt->execute();
            if ($row = $stmt->fetch(PDO::FETCH_ASSOC))
            {
                $book = new Book($row['title'], 
                                 $row['author'], 
                                 $row['description'], 
                                 $row['ID']);
            }
        }
        catch (PDOException $e)
        {
            echo 'PDOException: ' . $e->getMessage();
        }
        return $book;
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
        if (is_numeric($book->id) || is_null($book->id))
        {
            if (is_string($book->title) && is_string($book->author) && ($book->title != '') && ($book->author != ''))
            {
                $stmt = $this->db->prepare(
                        "INSERT INTO booklist (title, author, description) 
                         VALUES (:title, :author, :description);");
        
                $stmt->bindValue(':title', $book->title);
                $stmt->bindValue(':author', $book->author);
                $stmt->bindValue(':description', $book->description);
                try
                {
                    $stmt->execute();
                    $book->id = $this->db->lastInsertID();
                }
                catch (PDOException $e)
                {
                    echo 'PDOException: ' . $e->getMessage();
                }
            }
        }
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
        $stmt = $this->db->prepare(
            "UPDATE booklist
             SET title = :newtitle, author = :newauthor, description = :newdesc
             WHERE ID = :id;");

        $stmt->bindValue(':newtitle', $book->title);
        $stmt->bindValue(':newauthor', $book->author);
        $stmt->bindValue(':newdesc', $book->description);
        $stmt->bindValue(':id', $book->id);

        try 
        {
            $stmt->execute();        
        } 
        catch (PDOException $e) 
        {
            echo 'PDOException: ' . $e->getMessage();
        }
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
        if (!is_numeric($id))
        {
            $id = -1;
        }

        $stmt = $this->db->prepare(
            "DELETE FROM booklist
             WHERE ID = :id;");

        $stmt->bindValue(':id', $id);

        try 
        {
            $stmt->execute();
        } 
        catch (PDOException $e) 
        {
            echo 'PDOException: ' . $e->getMessage();
        }
    }
	
}

?>